I'd like to request several samples for X->bb tagger development:

 * DSID 3610{20..32} (JZW)
 * DSID 301{488..507} (G -> hh -> bbbb)
 * DSID 3013{22..35} (Z' -> tt)
 * DSID 3012{54..87} (W' -> WZ)
 * DSID 309450 (SM Higgs with pt cut)

The tags should be `s3126_r9364`. I've created full lists of [mc16a][1] and [mc16d][2] samples on gitlab.

Note that these need to be processed in release 21.2.35 (p-tag: `p3587`)

[1]: https://gitlab.cern.ch/danthings/xbb-datasets/raw/master/mc16a-containers.txt
[2]: https://gitlab.cern.ch/danthings/xbb-datasets/raw/master/mc16d-containers.txt

