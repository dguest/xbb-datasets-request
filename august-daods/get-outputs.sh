#!/usr/bin/env bash

set -eu

for CONT in $(cat ../mc16a-containers.txt ) ; do
    STUB="$(gsed -r  's/.*:(.*)\.recon\.AOD.*/\1/' <<< $CONT)*_p3596"
    pandamon -u 'atlas-dpd-production' -d 30 -s OUT ${STUB}
done
